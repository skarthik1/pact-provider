
import au.com.dius.pact.provider.junit.PactRunner;

import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.loader.PactBroker;
import au.com.dius.pact.provider.junit.target.HttpTarget;
import au.com.dius.pact.provider.junit.target.Target;
import au.com.dius.pact.provider.junit.target.TestTarget;
import org.junit.runner.RunWith;

@RunWith(PactRunner.class)
@Provider("pact_post_provider")
@PactBroker
public class PostContractTest {
    @TestTarget
    public final Target target = new HttpTarget("http", "dummy.restapiexample.com", 80);
}
